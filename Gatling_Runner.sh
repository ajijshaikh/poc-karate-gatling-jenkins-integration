#!/bin/bash

echo $1

x=$(cat < $1)

echo ${x}

while read user duration feature tag load_simulation ; do
    echo "$user"
    echo "$duration"
    echo "$feature"
    echo "$tag"
    echo "$load_simulation"

curl -X POST -u admin:admin "http://localhost:9999/job/gatling/buildWithParameters?users=${user}&duration=$duration&feature=$feature&tag=$tag&load_simulation=$load_simulation" 


done < <(echo "$x" | jq -r '.Config[]|"\(.User) \(.Duration) \(.Feature) \(.Tag) \(.Load_simulation)"')

